'use client';
import React, { Suspense, useContext } from 'react';
import { useSearchParams } from 'next/navigation';
import { LayoutContext } from '../layout/context/layoutcontext';
import { Card } from 'primereact/card';
import Image from 'next/image';

const IndexContent: React.FC = () => {
    const { basePatch } = useContext(LayoutContext);
    const searchParams = useSearchParams();
    const welcomeParam = searchParams.get('welcome');

    return (
        <Card className='col-12 md:col-12 border-round-xl shadow-1 mb-5 pointer-events-none select-none h-auto' header={
            <div className='col-12 flex flex-wrap justify-content-center p-0'>
                <div className='flex justify-content-center align-items-center col-12'>
                    <label className='font-bold m-0 text-7xl text-center'>¡Bienvenidos a APA! {welcomeParam}</label>
                </div>
            </div>
        }>
            <div className='col-12 overflow-hidden flex justify-content-center'>
                <Image
                    src={`${basePatch}/img/hcsba.png`}
                    alt='hero-1'
                    width={400}
                    height={400}
                    className='w-max h-max ml-4 my-0 py-0'
                    style={{ scale: '100%' }}
                />
            </div>
        </Card>
    );
};

const Index: React.FC = () => {
    return (
        <Suspense fallback={<div>Loading...</div>}>
            <IndexContent />
        </Suspense>
    );
};

export default Index;
