'use client';
import React, { Suspense } from 'react';

const EmptyPageContent: React.FC = () => {
    return (
        <div className="grid">
            <div className="col-12">
                <div className="card">
                    <h5>Empty Page</h5>
                    <p>Use this page to start from scratch and place your custom content.</p>
                </div>
            </div>
        </div>
    );
};

const EmptyPage: React.FC = () => {
    return (
        <Suspense fallback={<div>Loading...</div>}>
            <EmptyPageContent />
        </Suspense>
    );
};

export default EmptyPage;
