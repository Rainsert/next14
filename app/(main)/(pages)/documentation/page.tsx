'use client';
import React, { Suspense } from 'react';
import { useSearchParams } from 'next/navigation';

const DocumentationContent: React.FC = () => {
    const searchParams = useSearchParams();
    const paramValue = searchParams.get('docParam');

    return (
        <div className="grid">
            <div className="col-12">
                <div className="card docs">
                    <h4>Current Version</h4>
                    <p>Next v13, React v18, Typescript with PrimeReact v10</p>
                    <p>Value of docParam: {paramValue}</p>
                    {/* Resto del contenido */}
                </div>
            </div>
        </div>
    );
};

const Documentation: React.FC = () => {
    return (
        <Suspense fallback={<div>Loading...</div>}>
            <DocumentationContent />
        </Suspense>
    );
};

export default Documentation;
